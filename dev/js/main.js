/*! [PROJECT_NAME] | Suitmedia */

;(function ( window, document, undefined ) {

    var path = {
        css: myPrefix + 'assets/css/',
        js : myPrefix + 'assets/js/vendor/'
    };

    var assets = {
        _jquery_cdn     : 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js',
        _jquery_local   : path.js + 'jquery.min.js',
        _fastclick      : path.js + 'fastclick.min.js'
    };

    var Site = {

        init: function () {
            Site.fastClick();
            Site.enableActiveStateMobile();
            Site.WPViewportFix();
            Site.modal();

            window.Site = Site;
        },

        fastClick: function () {
            Modernizr.load({
                load    : assets._fastclick,
                complete: function () {
                    FastClick.attach(document.body);
                }
            });
        },

        enableActiveStateMobile: function () {
            if ( document.addEventListener ) {
                document.addEventListener('touchstart', function () {}, true);
            }
        },

        WPViewportFix: function () {
            if ( navigator.userAgent.match(/IEMobile\/10\.0/) ) {
                var style   = document.createElement("style"),
                    fix     = document.createTextNode("@-ms-viewport{width:auto!important}");

                style.appendChild(fix);
                document.getElementsByTagName('head')[0].appendChild(style);
            }
        },

        modal: function() {
            var content = $('.article-hidden').html();
            var $body = $('body');

            $body.append('<div class="modal"></div>');
            $('.modal').append('<div class="modal-container"></div>');
            $('.modal-container').append('<button class="modal-close-btn btn">&times;</button>');
            $('.modal-container').append('<div class="modal-content"></div>');

            
            var $clickPop = $('.triggerpop');
            var $modal = $('.modal');
            var disableScroll = 'disable-scroll';        

            $clickPop.click(function(event) {
                event.preventDefault();
                var target = $(event.target).attr('data-target');
                var content = $(target).html();
                $('.modal-content').html(content);
                $modal.fadeIn();
                $body.removeClass(disableScroll);
            });

            $('.modal-close-btn').on('click', function(event) {
                event.preventDefault();
                $modal.fadeOut();
                $body.removeClass(disableScroll); 
            });

            $(document).on('keydown',function(event) {
                if (event.keyCode === 27) {
                    $modal.fadeOut();
                    $body.removeClass(disableScroll);
                }
            });

            $modal.on('click', function(event) {
                event.preventDefault();
                $modal.fadeOut();
                $body.removeClass(disableScroll);
            });
        }
    };

    var checkJquery = function () {
        Modernizr.load([
            {
                test    : window.jQuery,
                nope    : assets._jquery_local,
                complete: Site.init
            }
        ]);
    };

    Modernizr.load({
        load    : assets._jquery_cdn,
        complete: checkJquery
    });

})( window, document );
